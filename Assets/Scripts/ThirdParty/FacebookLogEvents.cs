﻿using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;
using KnockBalls.Common;
using System;

public class FacebookLogEvents : MonoBehaviour, MMEventListener<MMGameEvent>
{
    void Awake()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            //Handle FB.Init
            FB.Init(() => {
                FB.ActivateApp();
            });
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pauseStatus)
        {
            //app resume
            if (FB.IsInitialized)
            {
                FB.ActivateApp();
            }
            else
            {
                //Handle FB.Init
                FB.Init(() => {
                    FB.ActivateApp();
                });
            }
        }
    }

    public void LogAchieveLevelEvent(string level)
    {
        var parameters = new Dictionary<string, object>();
        parameters[AppEventParameterName.Level] = level;
        FB.LogAppEvent(AppEventName.AchievedLevel, null, parameters);
    }

    public void OnMMEvent(MMGameEvent eventType)
    {
        if (eventType.EventName == "LevelComplete")
        {
            string name = Convert.ToString(eventType.Arg);
            LogAchieveLevelEvent(name);
        }
    }

    private void OnEnable()
    {
        this.MMEventStartListening<MMGameEvent>();
    }

    private void OnDisable()
    {
        this.MMEventStopListening<MMGameEvent>();
    }
}
