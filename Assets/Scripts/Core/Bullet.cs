﻿using KnockBalls.Game;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
    private Rigidbody rb;

    private AutoDestory destroer;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        destroer = GetComponent<AutoDestory>();
    }

    /// <summary>
    /// Начинает полет снаряда
    /// </summary>
    /// <param name="velocity"></param>
    public void StartFly(Vector3 velocity)
    {
        rb.velocity = velocity;
        destroer?.AutoDestroy();
    }
}
