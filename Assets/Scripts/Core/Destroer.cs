﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroer : MonoBehaviour, IDestroer
{
    public void Destroy(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }
}
