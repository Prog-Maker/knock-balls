﻿
using UnityEngine;

public interface IDestroer 
{
    void Destroy(GameObject gameObject);
}
