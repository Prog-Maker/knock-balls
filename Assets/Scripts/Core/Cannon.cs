﻿using System.Collections;
using UnityEngine;
using KnockBalls.Common;

namespace KnockBalls.Core
{
    public class Cannon : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private float rayDistance = 20f;

        [SerializeField]
        private float rollbakInterval = 0.1f;

        [SerializeField]
        private float timeToTarget = 0.5f;

        [Tooltip("Ограничение на поворот пушки")]
        [SerializeField]
        private float clampYmin, clampYmax, clampXmin, clampXmax;

        [SerializeField]
        private Transform shootPoint;

        [SerializeField]
        private BulletPool bulletPool;

        private new Camera camera;

        private bool canShoot = true;

        private float shootInterval;

        private void Start()
        {
            camera = Camera.main;
        }

        void Update()
        {
            LaunchProjectile();
        }

        /// <summary>
        /// Поворачивает пушку и запускает снаряд
        /// </summary>
        private void LaunchProjectile()
        {
            if (shootInterval < rollbakInterval)
            {
                shootInterval += Time.deltaTime;
            }

            if (Input.GetMouseButtonDown(0) && canShoot)
            {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out var hit, rayDistance, layerMask))
                {
                    Vector3 Vo = CalculateVelocity(hit.point, shootPoint.position, timeToTarget);

                    RotateToTarget(hit.point);

                    if (shootInterval >= rollbakInterval)
                    {
                        Shoot(Vo);
                        shootInterval = 0.0f;
                        MMGameEvent.Trigger("Shoot");
                    }
                }
            }
        }

        /// <summary>
        /// Расчитывает скорость снаряда
        /// </summary>
        /// <param name="target">Позиция цели</param>
        /// <param name="origin">Позиция пуска снаряда</param>
        /// <param name="time">Время полета снаряда</param>
        /// <returns></returns>
        private Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
        {
            //define the distance x and y first;
            Vector3 distance = target - origin;
            Vector3 distanceXZ = distance;
            distanceXZ.y = 0f;

            //create a float represent our distance
            float Sy = distance.y;
            float Sxz = distanceXZ.magnitude;

            float Vxz = Sxz / time;
            float Vy = Sy / time + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

            Vector3 result = distanceXZ.normalized;
            result *= Vxz;
            result.y = Vy;

            return result;
        }

        /// <summary>
        /// Поворачивает пушку в сторону цели
        /// </summary>
        /// <param name="target"></param>
        private void RotateToTarget(Vector3 target)
        {
            target.x = Mathf.Clamp(target.x, clampXmin, clampXmax);
            target.y = Mathf.Clamp(target.y, clampYmin, clampYmax);
            transform.LookAt(target, Vector3.up);
        }

        /// <summary>
        /// Детает выстрел
        /// </summary>
        /// <param name="velovity">скорость снаряда</param>
        private void Shoot(Vector3 velovity)
        {
            Bullet obj = bulletPool.GetFromPool();

            if (obj)
            {
                StartCoroutine(Rollback());
                obj.gameObject.SetActive(true);
                obj.transform.position = shootPoint.position;
                obj.transform.rotation = Quaternion.identity;
                obj.StartFly(velovity);
            }
        }

        /// <summary>
        /// Делает откат пушки
        /// </summary>
        /// <returns></returns>
        IEnumerator Rollback()
        {
            var startPos = transform.position;
            var endPos = startPos;
            endPos.z -= 0.2f;

            for (float t = 0; t <= 1 * rollbakInterval; t += Time.deltaTime)
            {
                transform.position = Vector3.Lerp(startPos, endPos, t / rollbakInterval);
                yield return null;
            }
            for (float t = 0; t <= 1 * rollbakInterval; t += Time.deltaTime)
            {
                transform.position = Vector3.Lerp(endPos, startPos, t / rollbakInterval);
                yield return null;
            }

            transform.position = startPos;
        }

        public void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.EventName == "StopShoot")
            {
                canShoot = false;
            }

            if(eventType.EventName == "CanShoot")
            {
                canShoot = true;
            }
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}
