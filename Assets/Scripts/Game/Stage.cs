﻿using UnityEngine;

namespace KnockBalls.Game
{
    [System.Serializable]
    public class Stage
    {
        public string Name;

        public GameObject stagePrefab;

        public bool Complete = false;

        public int CountShoots;
    }
}
