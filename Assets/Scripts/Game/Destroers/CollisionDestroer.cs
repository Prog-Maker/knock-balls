﻿using KnockBalls.Common;
using UnityEngine;

namespace KnockBalls.Game
{
    public class CollisionDestroer : MonoBehaviour
    {
        [SerializeField]
        private LayerMask targetLayer;
       
        
        IDestroer destroer;
        LevelManager levelManager;

        private void Start()
        {
            destroer = Give.Me<IDestroer>();
            levelManager = Give.Me<LevelManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if ((1 << other.gameObject.layer) == targetLayer.value)
            {
                levelManager.MinusTarget();
            }
            
            destroer.Destroy(other.gameObject);
        }
    }
}
