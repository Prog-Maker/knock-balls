﻿using UnityEngine;
using KnockBalls.Common;

namespace KnockBalls.Game
{
    public class AutoDestory : MonoBehaviour
    {
        [SerializeField]
        private float destroyAfterSecond = 3f;

        private IDestroer destroer;

        private void Awake()
        {
            destroer = Give.Me<IDestroer>();
        }

        public void AutoDestroy()
        {
            Invoke(nameof(Destroy), destroyAfterSecond);
        }

        public void Destroy()
        {
            destroer.Destroy(gameObject);
        }
    }
}
