﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KnockBalls.Common;
namespace KnockBalls.Game
{
    public class GameManager : PersistentSingleton<GameManager>
    {
        private LevelManager levelManager;
        
        protected override void Awake()
        {
            base.Awake();
            Give.Them<IDestroer, Destroer>();
            Give.Them(Instance);
            Give.Them<LevelManager, LevelManager>();
        }

        void Start()
        {
            levelManager = Give.Me<LevelManager>();
            LoadLastLevelCompleted();
        }


        private void LoadLastLevelCompleted()
        {
            int index = levelManager.GetUnCompleteLevelIndex();
            levelManager.SetCurrentLevel(index);
            levelManager.CreateLevel();
        }
    }
}
