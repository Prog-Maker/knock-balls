﻿using KnockBalls.Common;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace KnockBalls.Game
{
    public class GuiManager : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        [Header("Level UI Elements")]
        [SerializeField]
        private Text currentLevelTextBox;

        [SerializeField]
        private Text nextLevelTextBox;

        [SerializeField]
        private Image[] stagesImages;

        [Header("Shoots UI Elements")]
        [SerializeField]
        private Text shootCountTextBox;

        [Header("Reload Level UI Elements")]
        [SerializeField]
        private Text reloadTimeTextBox;

        private LevelManager levelManager;

        private LevelManager LevelManager
        {
            get
            {
                if (levelManager == null)
                {
                    levelManager = Give.Me<LevelManager>();
                    return levelManager;
                }
                
                return levelManager;
            }
        }

        void Start()
        {
            InitLevelUI();
        }

        private void InitLevelUI()
        {
            currentLevelTextBox.text = (LevelManager.CurrentLevel + 1).ToString();
           
            nextLevelTextBox.text = (LevelManager.CurrentLevel + 2).ToString();

            var stages = LevelManager.GetCurrentLevel.Stages;

            for (int i = 0; i < stages.Length; i++)
            {
                if (stages[i].Complete)
                {
                    stagesImages[i].color = Color.green;
                }
                else
                {
                    stagesImages[i].color = Color.gray;
                }

                if (i == LevelManager.CurrentStage)
                {
                    stagesImages[i].color = Color.yellow;
                }
            }
        }

        private void UpdateShootsUI()
        {
            shootCountTextBox.text = LevelManager.CountShootsLeft.ToString();
        }

        private void UpdateLevelUI()
        {
            InitLevelUI();
        }

        private void UpdateTimeLeftToReload(int value)
        {
            reloadTimeTextBox.text = value.ToString();
        }

        private void StopAndCloseEndTimer()
        {
            StopAllCoroutines();
            reloadTimeTextBox.gameObject.SetActive(false);
        }

        private IEnumerator ShowTimer(int value)
        {
            reloadTimeTextBox.gameObject.SetActive(true);

            WaitForSeconds wait = new WaitForSeconds(1);

            for (int i = value; i >= 0; i--)
            {
                UpdateTimeLeftToReload(i);
                yield return wait;
            }

            reloadTimeTextBox.gameObject.SetActive(false);
        }

        public void OnMMEvent(MMGameEvent eventType)
        {
            switch (eventType.EventName)
            {
                case "UpdateState":
                    UpdateLevelUI();
                    StopAndCloseEndTimer();
                    break;
                case "UpdateShoots":
                    UpdateShootsUI();
                    break;
                case "UpdateTimeToEnd":
                    int value = Convert.ToInt32(eventType.Arg);
                    StartCoroutine(ShowTimer(value));
                    break;
            }
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}
