﻿using UnityEngine;
using System.Linq;
using KnockBalls.Common;
using System.Collections;

namespace KnockBalls.Game
{
    public class LevelManager : MonoBehaviour, MMEventListener<MMGameEvent>
    {
        [SerializeField]
        private Level[] levels;

        [SerializeField]
        private int currentStageIndex = 0;

        [SerializeField]
        private int currentLevelIndex = 0;

        [SerializeField]
        private int countShoots;

        [SerializeField]
        private Transform stagePosition;

        [Tooltip("In Seconds")]
        [SerializeField]
        private float waitTimeForResetLevel = 5;

        private int countTargets = 0;

        public int CurrentLevel => currentLevelIndex;

        public int CurrentStage => currentStageIndex;

        public int CountShootsLeft => countShoots;

        public Level GetCurrentLevel => levels[currentLevelIndex];

        public Stage GetCurrentStage => GetCurrentLevel.Stages[currentStageIndex];

        private GameObject GetLevelPrefab => GetCurrentStage.stagePrefab;

        private GameObject currentStageObject;

        private const string UPDATE_SHOOTS = "UpdateShoots";
        private const string UPDATE_STATE = "UpdateState";
        private const string UPDATE_TIME_TO_END_LEVEL = "UpdateTimeToEnd";
        private const string STOP_SHOOT = "StopShoot";
        private const string CAN_SHOOT = "CanShoot";
        private const string LEVEL_COMLPETE = "LevelComplete";

        public void SetCurrentLevel(int value)
        {
            currentLevelIndex = value;
        }

        public void CreateLevel()
        {
            ResetStages();
            CreateStage();
        }

        public void ReloadLevel()
        {
            ResetStages();

            Destroy(currentStageObject);
            CreateStage();

            MMGameEvent.Trigger(UPDATE_SHOOTS);
            MMGameEvent.Trigger(UPDATE_STATE);
        }

        public void MinusTarget()
        {
            countTargets -= 1;

            if (countTargets == 0)
            {
                if (currentStageIndex + 1 < GetCurrentLevel.Stages.Length)
                {
                    currentStageIndex++;
                    GetCurrentLevel.Stages[currentStageIndex - 1].Complete = true;

                    MMGameEvent.Trigger(UPDATE_STATE);

                    Destroy(currentStageObject);
                    CreateStage();
                }
                else
                {
                    GetCurrentLevel.Stages[currentStageIndex].Complete = true;

                    MMGameEvent.Trigger(UPDATE_STATE);
                    MMGameEvent.Trigger(LEVEL_COMLPETE, GetCurrentLevel.Name);

                    NextLevel();
                }
            }
        }

        public void MinusShoots()
        {
            countShoots--;

            if (countShoots == 0)
            {
                MMGameEvent.Trigger(STOP_SHOOT);
                StartCoroutine(WaitUntilAllGoalsFall());
            }

            MMGameEvent.Trigger(UPDATE_SHOOTS);
        }

        public int GetUnCompleteLevelIndex()
        {
            for (int i = 0; i < levels.Length; i++)
            {
                if (!levels[i].Complete)
                {
                    return i;
                }
            }

            return 0;
        }

        public void OnMMEvent(MMGameEvent eventType)
        {
            if (eventType.EventName == "Shoot")
            {
                MinusShoots();
            }
        }

        private void CreateStage()
        {
            StopAllCoroutines();

            GameObject stagePrefab = GetLevelPrefab;

            currentStageObject = Instantiate(stagePrefab);

            GetCurrentStage.Complete = false;

            currentStageObject.transform.SetParent(stagePosition);

            countTargets = currentStageObject.GetComponentsInChildren<Rigidbody>()
                                       .Where(r => r.isKinematic == false).Count();

            countShoots = (currentStageIndex + 1) * 3;

            MMGameEvent.Trigger(CAN_SHOOT);
            MMGameEvent.Trigger(UPDATE_SHOOTS);
        }

        private void ResetStages()
        {
            foreach (var stage in GetCurrentLevel.Stages)
            {
                stage.Complete = false;
            }

            currentStageIndex = 0;
        }

        private IEnumerator WaitUntilAllGoalsFall()
        {
            var waiter = new WaitForSeconds(waitTimeForResetLevel);

            MMGameEvent.Trigger(UPDATE_TIME_TO_END_LEVEL, waitTimeForResetLevel);
           
            yield return waiter;

            ReloadLevel();
        }

        private void NextLevel()
        {
            if ((CurrentLevel + 1) < levels.Length)
            {
                levels[currentLevelIndex].Complete = true;
                currentLevelIndex++;
                UpdateLevel();
            }
            else
            {
                currentLevelIndex = 0;
                foreach (var level in levels)
                {
                    level.Complete = false;
                }
                UpdateLevel();
            }
        }

        private void UpdateLevel()
        {
            currentStageIndex = 0;
            Destroy(currentStageObject);
            CreateLevel();

            MMGameEvent.Trigger(UPDATE_STATE);
        }

        private void OnEnable()
        {
            this.MMEventStartListening<MMGameEvent>();
        }

        private void OnDisable()
        {
            this.MMEventStopListening<MMGameEvent>();
        }
    }
}