﻿using UnityEngine;

namespace KnockBalls.Game
{
    public class Level : MonoBehaviour
    {
        public string Name;

        public Stage[] Stages;

        public bool Complete = false;
    }
}
