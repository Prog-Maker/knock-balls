﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;

namespace KnockBalls.Common
{
    /// <summary>
    /// I am IoC container.
    /// </summary>
    public static class Give
    {
        private static readonly Dictionary<Type, Type> Factories = new Dictionary<Type, Type>();
        private static readonly Dictionary<Type, object> Container = new Dictionary<Type, object>();

        /// <summary>
        /// Ask for an instance of T that comes from anywhere.
        /// </summary>
        public static T Me<T>()
        {
            return (T)Me(typeof(T));
        }

        /// <summary>
        /// Register a K type for T type for a lazy creation.
        /// </summary>
        public static void Them<T, TK>() where TK : class, T
        {
            Factories.Add(typeof(T), typeof(TK));
        }

        /// <summary>
        /// Register an instance to give it to anything.
        /// </summary>
        public static void Them<T>(T item)
        {
            var type = typeof(T);

            Assert.IsFalse(
                Container.ContainsKey(type),
                $"Replacing type {type}");

            Container[type] = item;
        }

        public static void None<T>()
        {
            None(typeof(T));
        }

        public static void Clear()
        {
            foreach (var type in Container.Keys.ToArray())
            {
                None(type);
            }

            Factories.Clear();
        }

        private static object Me(Type type)
        {
            if (Container.TryGetValue(type, out var result))
            {
                return result;
            }

            result =  TryFindOnScene(type) ?? TryCreateFromFactory(type);

            Assert.IsNotNull(result, $"Couldn't resolve {type}");

            Container.Add(type, result);

            return result;
        }

        private static object TryCreateFromFactory(Type type)
        {
            if (!Factories.TryGetValue(type, out var factory))
            {
                return null;
            }

            var constructors = factory.GetConstructors();

            Assert.AreEqual(1, constructors.Length);

            var injections = constructors
                .First()
                .GetParameters()
                .Select(item => item.ParameterType)
                .Select(Me)
                .ToArray();

            return Activator.CreateInstance(factory, injections);
        }

        private static object TryFindOnScene(Type type)
        {
            return Object
                .FindObjectsOfType<MonoBehaviour>()
                .FirstOrDefault(item => item.GetComponent(type))
                ?.GetComponent(type);
        }

        private static void None(Type type)
        {
            // check disposable -> dispose
            Container.Remove(type);
        }
    }
}
