﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    [SerializeField]
    private Vector3 rotationEulers;
    
    void Update()
    {
        transform.Rotate(rotationEulers);
    }
}
